PS1="\u@\h \w$ "

# Aliases

alias ll='ls -alh'
alias x='exit'
alias off='doas shutdown -p now'
alias reboot='doas shutdown -r now'
alias reset='doas shutdown -r now'
alias wall='/usr/local/bin/feh -z --bg-fill /home/joe/.wallpapers/*'
alias suspend='apm -S'
alias net='doas sh /etc/netstart iwn0'
alias log-listen='doas tcpdump -n -e -ttt -i pflog0'
alias xma='xmahjongg --tileset real --bg w'

export TERM=xterm-256color
export PATH=$PATH:/home/joe/.local/bin/

